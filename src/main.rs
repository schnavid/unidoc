#![feature(try_trait)]

#[macro_use]
extern crate lazy_static;

pub mod document;
pub mod parser;

fn main() {
    println!("Hello, world!");
}
