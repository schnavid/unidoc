use crate::document::attributes::AttributeEntry;

#[derive(Default)]
pub struct Header {
    title: Option<String>,
    authors: Vec<(String, String)>,

    attribute_entries: Vec<AttributeEntry>,
}

impl Header {
    pub fn set_title<S: Into<String>>(&mut self, title: S) -> bool {
        if self.title.is_none() {
            self.title = Some(title.into());
            true
        } else {
            false
        }
    }

    pub fn add_attribute_entry(&mut self, attribute_entry: AttributeEntry) {
        self.attribute_entries.push(attribute_entry);
    }
}