use crate::parser::{Parser, ParseResult};
use crate::document::header::Header;
use crate::parser::ParseResult::Success;

impl Parser {
    pub fn parse_header(&mut self) -> ParseResult<Header> {
        let mut header = Header::default();

        while let Some(attr) = self.parse_attribute_entry()? {
            header.add_attribute_entry(attr);
            self.skip_comments();
        }

        if let Some(title) = self.parse_title()? {
            header.set_title(title);
            self.skip_comments();
        }

        Success(header)
    }

    pub fn parse_title(&mut self) -> ParseResult<String> {
        todo!()
    }
}