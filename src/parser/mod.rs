use crate::document::Document;
use std::ops::Try;

pub mod header;
pub mod attributes;

pub struct Parser {
    lines: Vec<String>,
    current_line: usize,
}

impl Parser {
    pub fn new<S: Into<String>>(input: S) -> Parser {
        Parser {
            lines: input.into().lines().map(str::to_owned).collect(),
            current_line: 0,
        }
    }

    pub fn peek(&self) -> Option<&String> {
        self.lines.get(self.current_line)
    }

    pub fn skip(&mut self) {
        self.current_line += 1;
    }

    pub fn skip_blank_lines(&mut self) {
        while let Some(x) = self.peek() {
            let x = x.trim();
            if !x.is_empty() && !x.starts_with("//") {
                break;
            }

            self.skip();
        }
    }

    pub fn skip_comments(&mut self) {
        while let Some(x) = self.peek() {
            let x = x.trim();
            if !x.starts_with("//") {
                break;
            }

            self.skip();
        }
    }

    pub fn parse(&mut self) -> ParseResult<Document> {
        self.skip_blank_lines();

        let header = self.parse_header()?;

        todo!()
    }
}

pub struct ParseError {
    kind: ErrorKind,
    line: u16,
    pos: Option<u16>,
}

impl ParseError {
    pub fn new(kind: ErrorKind, line: u16) -> ParseError {
        ParseError {
            kind,
            line,
            pos: None
        }
    }
}

pub enum ErrorKind {
    Unknown,
    NonLevel0SectionTitleInHeader,
}

impl Default for ParseError {
    fn default() -> Self {
        ParseError {
            kind: ErrorKind::Unknown,
            line: 0,
            pos: None
        }
    }
}

pub enum ParseResult<T> {
    Success(T),
    Error(ParseError),
    Failure(ParseError),
}

impl<T> Try for ParseResult<T> {
    type Ok = Option<T>;
    type Error = ParseError;

    fn into_result(self) -> Result<Self::Ok, <Self as Try>::Error> {
        match self {
            ParseResult::Success(x) => Ok(Some(x)),
            ParseResult::Error(x) => Ok(None),
            ParseResult::Failure(x) => Err(x),
        }
    }

    fn from_error(v: <Self as Try>::Error) -> Self {
        Self::Failure(v)
    }

    fn from_ok(v: Self::Ok) -> Self {
        match v {
            None => Self::Error(ParseError::default()),
            Some(x) => Self::Success(x),
        }
    }
}